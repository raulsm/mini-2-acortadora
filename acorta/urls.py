from django.contrib import admin
from django.urls import include, path
from acorta.migrations import views

urlpatterns = [
    path('', views.index),
    path('<str:short>/', views.redirection),
]